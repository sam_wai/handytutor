<?php


include 'configs/connections.php';
include 'mainIncludes/header.html.php';

if(!isset($_SESSION['logged_user']))
     {
       $msg= 'Error: Kindly login in to continue';
       // header("location:loginRegister.php?msg=$msg");
       
       echo '<script type="text/javascript">'; 
       echo 'window.location =loginRegister.php?msg="+$msg;';
       echo '</script>';

    }	
?>



			<!-- CORE : begin -->
			<div id="core">

				<!-- PAGE TITLE : begin -->
				<div id="page-title" class="m-parallax">

					<!-- PAGE TITLE TOP : begin -->
					<div class="page-title-top">
						<div class="page-title-top-inner">

							<!-- PAGE TITLE TEXT : begin -->
							<div class="page-title-text">
								<div class="container">
									<h1>Post Homework</h1>
								</div>
							</div>
							<!-- PAGE TITLE TEXT : end -->

						</div>
					</div>
					<!-- PAGE TITLE TOP : end -->

					<!-- PAGE TITLE BOTTOM : begin -->
					<div class="page-title-bottom">
						<div class="container">
							<div class="page-title-bottom-inner">
								<div class="page-title-bottom-inner2">

									<!-- PAGE TITLE BREADCRUMBS : begin -->
									<div class="page-title-breadcrumbs">
										<ul>
											<li><a href="index.php">Home</a></li>
											<li><a href="">Post Homework</a></li>
										</ul>
									</div>
									<!-- PAGE TITLE BREADCRUMBS : end -->

								</div>
							</div>
						</div>
					</div>
					<!-- PAGE TITLE BOTTOM : end -->

				</div>
				<!-- PAGE TITLE : end -->

				<div class="container">
					<div class="row">
						<div class="col-md-9">

							<!-- PAGE CONTENT : begin -->
							<div id="page-content">

								<!-- BLOG DETAIL PAGE : begin -->
								<div class="blog-detail-page">

									<!-- ARTICLE : begin -->
									<article class="article-body">
										<div class="article-inner">
                                           <?php 
                                             if(isset($_REQUEST['msg']))
                                             {
                                             	echo '<div class="alert alert-success">';
                                             	echo '<a href="#" class="close" data-dismiss="alert">&times;</a>';
                                             	echo $_REQUEST['msg'];
                                             	echo '</div>';
                                             }
                                         
                                           ?>
											<!-- ARTICLE HEADER : begin -->
											<header class="article-header">
												<h2 class="article-title">Create Question</h2>
											</header>
											<!-- ARTICLE HEADER : end -->
											
										
										
										<!-- CONTACT FORM : begin -->
      
						<form id="contact-form" class="default-form" action="Submit.php" method="post" enctype="multipart/form-data">
					     	
					     	

							<input type="hidden" name="contact-form">

							<div class="row">

								<div class="col-sm-4">

									<!-- SUBJECT FIELD : begin -->
									<div class="form-field" style="display: inline-block;">
   
                                  <input type="text" placeholder="Select date" id="datepicker" name="end_date" readonly>
									</div>
									<!-- SUBJECT FIELD : end -->


								</div>
							</div>						
							<div class="row">
							
							
								<div class="col-sm-10">

										<!-- SUBJECT FIELD : begin -->
										<div class="form-field">
											<label for="contact-subject">Title:</label>
											<input type="text" id="contact-subject" name="ques_title" required>
										</div>
										<!-- SUBJECT FIELD : end -->

								</div>	
				
							 </div>
		
								
							<div class="row">
							
							  <div class="col-sm-11">
						
								<script type="text/javascript" src="library/js/nicEdit.js"></script>
								<script type="text/javascript">
									bkLib.onDomLoaded(function() { nicEditors.allTextAreas() });
								</script>
								
								<label for="contact-subject">Description</label>
								<textarea type="text" style="width: 91%;" name="ques_descr" required>
								
								</textarea>
								
								
							</div>
						
						
							</div>
							<br><br>
							<div class="row">
							 
							 <div class="col-sm-10">
							 <!-- CHECKBOX : begin -->
							<div class="form-field">
								<div class="checkbox-group">
									<p><label for="checkbox-1"><input id="checkbox-1" class="checkbox-input" type="checkbox" name="email_notf" value="1">Yes</label></p>
									<p>Would you like to receive notifications by email when users post answers to your question?</p>
								</div>
							</div>
							<!-- CHECKBOX : end -->
							</div>
							 </div>
							 
							 
							 <div class="row">
	
								<div class="col-sm-10">

										<!-- SUBJECT FIELD : begin -->
										<div class="form-field">
											<label for="contact-subject">Attachments:</label>
											<input type="file" id="contact-subject" name="file">
										</div>
										<!-- SUBJECT FIELD : end -->

								</div>	
				
							 </div>
							 
							 
							 <div class="row">
							 
							 <div class="col-sm-10">
							 <label for="select-level"><h5>Academic Level</h5></label>
											<select class="selectbox-input" name="study_level" required>
												<option value="High School">High school</option>
												<option value="Undergraduate"> Undergraduate</option>
												<option value="Master">Master</option>
												<option value="Ph.D">Ph.D</option>
											</select>
							 
							 </div>
							 
							 </div>
							 
							 
							 
							 
							 <div class="row">
							 
							 <div class="col-sm-10">
							 <label for="select-field"><h5>Field of study</h5></label>
											<select class="selectbox-input" name="study_field" required>
												<option value="1">Accounting, Business & Finance</option>
												<option value="2"> Agriculture and Horticulture</option>
												<option value="3"> Architecture, Building & Planning</option>
												<option value="4">Art and design</option>
												<option value="5">Biology</option>
												<option value="6">Chemistry</option>
												<option value="7">Communication and Media</option>
												<option value="8">Computing and IT</option>
												<option value="9">Economics</option>
												<option value="10">Education</option>
												<option value="11">Engineering</option>
												<option value="12">English Language & Literature</option>
												<option value="13">Geography</option>
												<option value="14">Health and Medicine</option>
												<option value="15">History</option>
												<option value="16">Human Resource Management</option>
												<option value="17">Hospitality and Catering</option>
												<option value="18">Law</option>
												<option value="19">Mathematics</option>
												<option value="20">Management</option>
												<option value="21">Marketing</option>
												<option value="22">Physics</option>
												<option value="23">Politics</option>
												<option value="24">Psychology and Counselling</option>
											</select>
							 
							 </div>
							 
							 </div>
							 
							 
							 <br>
							 <div class="row">
							 <div class="col-sm-5">
							 
							 <!-- SUBJECT FIELD : begin -->
							<div class="form-field">
								<label for="contact-subject"><h5>Put Number of pages e.g 2,3,10,15,10.5,11.5 etc (NB: we approximate that 1 page is 275 words)</h5></label>
								<input type="number" id="contact-subject" name="no_of_pages" required>
							</div>
							<!-- SUBJECT FIELD : end -->
							
							</div>
							 </div>
							 
							 
							 
							 <div class="row">
							 
							 <div class="col-sm-10">
							 <label for="select-style"><h5>Style</h5></label>
											<select class="selectbox-input" name="paper_style" required>
												<option value="APA">APA</option>
												<option value="MLA">MLA</option>
												<option value="Turbian">Turbian</option>
												<option value="Chicago">Chicago</option>
												<option value="Harvard">Harvard</option>
												<option value="Oxford">Oxford</option>
												<option value="Other">Other</option>
												<option value="Computing and IT">Computing and IT</option>
											</select>
							 
							 </div>
							 
							 </div>   
							 <br>
							  <div class="row">
                              					 <div class="">
                              					 <label for="select-1"><h5>Important Additional information</h5></label>
                              					 <textarea name="imp_info" style="width: 60%; height: 10%;">
                               					  
                              					 </textarea>
                            					
                              					 </div>
                             				</div>
                             				<br><br>
                             				<div class="row">
                             				<div class="form-field">
							<button class="c-button m-type-2 m-color-2" type="submit" data-label="Send Message" data-loading-label="Veryfying...">Preview &nbsp;<i class="fa fa-sign-in"></i></button>
							</div>
                             				</div>
                        
                                            
							</form
						<!-- CONTACT FORM : end -->
											
											
											

										</div>
									</article>
									<!-- ARTICLE : end -->

									<hr class="c-divider m-size-medium">

								

									
								</div>
								<!-- BLOG DETAIL PAGE : end -->

							</div>
							<!-- PAGE CONTENT : end -->

						</div>
						<div class="col-md-3">

							<hr class="c-divider m-size-large m-type-2 hidden-lg hidden-md">

							<!-- SIDEBAR : sidebar -->
							<div id="sidebar">
								<div class="sidebar-widget-list">

									<!-- SEARCH WIDGET : begin -->
									<div class="widget search-widget">
										<div class="widget-inner">
											<div class="widget-content">

												<form class="c-search-form" action="search.php">
													<div class="form-fields">
														<input type="text" placeholder="Search on site...">
														<button type="submit"><i class="fa fa-search"></i></button>
													</div>
												</form>

											</div>
										</div>
									</div>
									<!-- SEARCH WIDGET : end -->

									<!-- CATEGORIES WIDGET : begin -->
									<div class="widget links-widget">
										<div class="widget-inner">
											<h3 class="widget-title">Todays Reviews ...</h3>
											<ul>
												<li><a href="#">Fibinnacii.. <i class="fa fa-star "></i><i class="fa fa-star "></i><i class="fa fa-star "></i><i class="fa fa-star "></i><i class="fa fa-star "></i></a></li>
												<li><a href="#">Physics.. <i class="fa fa-star "></i><i class="fa fa-star "></i><i class="fa fa-star "></i><i class="fa fa-star "></i><i class="fa fa-star "></i></a></li>
												<li><a href="#">Comp sci 101.. <i class="fa fa-star "></i><i class="fa fa-star "></i><i class="fa fa-star "></i><i class="fa fa-star "></i><i class="fa fa-star "></i></a></li>
												<li><a href="#">Stats ,linear... <i class="fa fa-star "></i><i class="fa fa-star "></i><i class="fa fa-star "></i><i class="fa fa-star "></i><i class="fa fa-star "></i></a></li>
												<li><a href="#">Marketing que.. <i class="fa fa-star "></i><i class="fa fa-star "></i><i class="fa fa-star "></i><i class="fa fa-star "></i><i class="fa fa-star "></i></a></li>
												<li><a href="#">Comp sci 101.. <i class="fa fa-star "></i><i class="fa fa-star "></i><i class="fa fa-star "></i><i class="fa fa-star "></i><i class="fa fa-star "></i></a></li>
												<li><a href="#">Stats ,linear... <i class="fa fa-star "></i><i class="fa fa-star "></i><i class="fa fa-star "></i><i class="fa fa-star "></i><i class="fa fa-star "></i></a></li>
												<li><a href="#">Marketing que.. <i class="fa fa-star "></i><i class="fa fa-star "></i><i class="fa fa-star "></i><i class="fa fa-star "></i><i class="fa fa-star "></i></a></li>
												<li><a href="#">Comp sci 101.. <i class="fa fa-star "></i><i class="fa fa-star "></i><i class="fa fa-star "></i><i class="fa fa-star "></i><i class="fa fa-star "></i></a></li>
												<li><a href="#">More...</a></li>
											</ul>
										</div>
									</div>
									<!-- CATEGORIES WIDGET : end -->

									<!-- INSTAGRAM WIDGET : begin -->
									<div class="widget instagram-widget" data-id="241041378" data-limit="8">
										<div class="widget-inner">
											<h3 class="widget-title"><strong>Instagram</strong> Feed</h3>
											<div class="widget-content">

												<div class="c-loading-anim"><i class="fa fa-spinner fa-spin"></i></div>
												<div class="widget-feed"></div>

											</div>
										</div>
									</div>
									<!-- INSTAGRAM WIDGET : end -->

									<!-- TWITTER WIDGET : begin -->
									<div class="widget twitter-widget">
										<div class="widget-inner">
											<h3 class="widget-title">Twitter Feed</h3>
											<div class="widget-content">

												<!-- create your own Twitter widget at https://twitter.com/settings/widgets (you have to be logged in to your Twitter account) -->
												<a class="twitter-timeline" href="https://twitter.com/LubosBudkovsky" data-widget-id="552135621662474240">Tweets by @LubosBudkovsky</a>
												<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>

											</div>
										</div>
									</div>
									<!-- TWITTER WIDGET : end -->

								</div>
							</div>
							<!-- SIDEBAR : end -->

						</div>
					</div>
				</div>

			</div>
			 <script src="date_picker/pikaday.js"></script>
    <script>

    var picker = new Pikaday(
    {
        field: document.getElementById('datepicker'),
        firstDay: 1,
        minDate: new Date('2015-06-01'),
        maxDate: new Date('2020-12-31'),
        yearRange: [2015,2020]
    });
</script>
<!--calendar-->
<!--cookies-->

<!--cookies end-->


			<!-- CORE : end -->

			<?php

include 'mainIncludes/footer.html.php';

?>
