<?php

include 'mainIncludes/header.html.php';	
	
?>

			<!-- CORE : begin -->
			<div id="core">

				<!-- PAGE TITLE : begin -->
				<div id="page-title" class="m-parallax">

					<!-- PAGE TITLE TOP : begin -->
					<div class="page-title-top">
						<div class="page-title-top-inner">

							<!-- PAGE TITLE TEXT : begin -->
							<div class="page-title-text">
								<div class="container">
									<h1>Our Questions & Previous Answers</h1>
								</div>
							</div>
							<!-- PAGE TITLE TEXT : end -->

						</div>
					</div>
					<!-- PAGE TITLE TOP : end -->

					<!-- PAGE TITLE BOTTOM : begin -->
					<div class="page-title-bottom">
						<div class="container">
							<div class="page-title-bottom-inner">
								<div class="page-title-bottom-inner2">

									<!-- PAGE TITLE BREADCRUMBS : begin -->
									<div class="page-title-breadcrumbs">
										<ul>
											<li><a href="index.php">Home</a></li>
											<li><a href="">Previous Answers</a></li>
										</ul>
									</div>
									<!-- PAGE TITLE BREADCRUMBS : end -->

								</div>
							</div>
						</div>
					</div>
					<!-- PAGE TITLE BOTTOM : end -->

				</div>
				<!-- PAGE TITLE : end -->

				<div class="container">

					<!-- PAGE CONTENT : begin -->
					<div id="page-content">

						<!-- SEARCH RESULTS PAGE : begin -->
						<div class="search-results-page">

							<!-- SEARCH FORM : begin -->
							<!-- <form class="c-search-form" action="search.php">
								<div class="form-fields">
									<input type="text" data-placeholder="Lorem Ipsum">
									<button type="submit"><i class="fa fa-search"></i></button>
								</div>
							</form> -->
							<!-- SEARCH FORM : end -->

							<hr class="c-divider m-size-medium">
							<!-- FORM SECTION : begin -->
						<section class="c-section">

						<!-- 	<h2>Featured questions</h2>
							<h4>Answers are likely to be purchased by several students</h4> -->
							

							<form class="default-form" method="post" action="">
								<div class="row">
									<div class="col-md-6">

										<!-- SELECTBOX : begin -->
										<div class="form-field">
											 <label for="select-field"><h5><b>Enter Field of study to Search:</b></h5></label>
											<select class="selectbox-input" name="study_field" required>
												<option value="1">Accounting, Business & Finance</option>
												<option value="2"> Agriculture and Horticulture</option>
												<option value="3"> Architecture, Building & Planning</option>
												<option value="4">Art and design</option>
												<option value="5">Biology</option>
												<option value="6">Chemistry</option>
												<option value="7">Communication and Media</option>
												<option value="8">Computing and IT</option>
												<option value="9">Economics</option>
												<option value="10">Education</option>
												<option value="11">Engineering</option>
												<option value="12">English Language & Literature</option>
												<option value="13">Geography</option>
												<option value="14">Health and Medicine</option>
												<option value="15">History</option>
												<option value="16">Human Resource Management</option>
												<option value="17">Hospitality and Catering</option>
												<option value="18">Law</option>
												<option value="19">Mathematics</option>
												<option value="20">Management</option>
												<option value="21">Marketing</option>
												<option value="22">Physics</option>
												<option value="23">Politics</option>
												<option value="24">Psychology and Counselling</option>
											</select>
											<br>
											<!-- TEXT : begin -->
										<!-- <div class="form-field">
											<label for="text-1"> (*Optional) Username Of original student to ask the question</label>
											<input id="text-1" class="m-required" type="text" data-placeholder="Add text" placeholder="e.g john" name="stud_name">
										</div> -->
										<!-- TEXT : end -->
										
										<div class="ol-md-6">
									<p>

										<!-- DEFAULT BUTTON / COLOR 2 : begin -->
										<button class="c-button m-type-2 m-color-2" type="submit" name="que_search">Search &nbsp;<i class="fa fa-search"></i></button>
										<!-- DEFAULT BUTTON / COLOR 2 : end -->

									</p>
								</div>
								
										
										</div>
										<!-- SELECTBOX : end -->

									</div>
								</div>
							</form>

						</section>
						<!-- FORM SECTION : end -->
						
						<?php
                            if(isset($_POST['que_search']) || isset($_REQUEST['id'])){
                            	$id=$_REQUEST['id'];
                                $id=$_POST['study_field'];	

						            $query="SELECT *
						                    FROM question
						                    INNER JOIN users
						                    ON question.user_id=users.user_id
						                    -- INNER JOIN field_of_study
						                    -- ON question.field_id=field_of_study.field_id
						                    WHERE field_id='$id'";

						            $query2="SELECT *
						                    FROM field_of_study
						                    WHERE field_id='$id'";


						                $result = mysql_query($query);
						                $result2 = mysql_query($query2);

						                while($row=mysql_fetch_array($result2)){
                                           $study_field=$row['field'];

						                }

						                $num_rows_returned  = mysql_num_rows($result);
						                	
						                    if($num_rows_returned == 0)
						                         {
						                         echo "<div id='alert_login'>*No results found";
						                         }

						                    else{
                                             echo '<table>
														<thead>
															<tr>
																<th width="20%">Question</th>
																<th>Field of study</th>
																<th width="18%">Academic Level</th>
																
																<th>Posted On</th>
																<th>Posted By</th>
															</tr>
														</thead>
												</table>';
						                     while($row=mysql_fetch_array($result))
						                     {
						                      $title=$row['question'];
						                      
						                      $level=$row['study_level'];
						                      
						                      $date=$row['posted_date'];
						                      $asker=$row['username'];
						                  ?>
						                 <table class="table table-striped table-hover"> 
						                   <tbody><tr><td width="18%"><strong><?php echo '<a href="viewanswers.php?id=' .urlencode($row[0]). '">'. htmlspecialchars($title) .'</a>';?></strong></td><td><?php echo $study_field;?></td><td width="18%"><?php echo $level;?></td><td width="19%"><?php echo $date;?></td><td width="17%"><?php echo $asker;?></td></tr></tbody>
						                   
						                 </table>
						              <?php 
						             }
						           }
						         }
						        ?>


							<!-- PAGINATION : begin -->
							<!-- <div class="c-pagination">
								<ul>
									<li class="pagination-prev"><a href="#" class="c-button m-outline"><i class="fa fa-chevron-left"></i></a></li>
									<li><a href="#" class="c-button m-outline">1</a></li>
									<li class="m-active"><a href="#" class="c-button m-outline">2</a></li>
									<li><a href="#" class="c-button m-outline">3</a></li>
									<li class="pagination-next"><a href="#" class="c-button m-outline"><i class="fa fa-chevron-right"></i></a></li>
								</ul>
							</div> -->
							<!-- PAGINATION : end -->

						</div>
						<!-- SEARCH RESULTS PAGE : end -->

					</div>
					<!-- PAGE CONTENT : end -->

				</div>

			</div>
			<!-- CORE : end -->

			<?php

include 'mainIncludes/footer.html.php';

?>
