<?php

include 'mainIncludes/header.html.php';	
include 'configs/connections.php';
	
?>

			<!-- CORE : begin -->
			<div id="core">

				<!-- PAGE TITLE : begin -->
				<div id="page-title" class="m-parallax">

					<!-- PAGE TITLE TOP : begin -->
					<div class="page-title-top">
						<div class="page-title-top-inner">

							<!-- PAGE TITLE TEXT : begin -->
							<div class="page-title-text">
								<div class="container">
									<h1>Our prices</h1>
								</div>
							</div>
							<!-- PAGE TITLE TEXT : end -->

						</div>
					</div>
					<!-- PAGE TITLE TOP : end -->

					<!-- PAGE TITLE BOTTOM : begin -->
					<div class="page-title-bottom">
						<div class="container">
							<div class="page-title-bottom-inner">
								<div class="page-title-bottom-inner2">

									<!-- PAGE TITLE BREADCRUMBS : begin -->
									<div class="page-title-breadcrumbs">
										<ul>
											<li><a href="index.php">Home</a></li>
											<li><a href="">Our Prices</a></li>
										</ul>
									</div>
									<!-- PAGE TITLE BREADCRUMBS : end -->

								</div>
							</div>
						</div>
					</div>
					<!-- PAGE TITLE BOTTOM : end -->

				</div>
				<!-- PAGE TITLE : end -->

				<div class="container">

					<!-- PAGE CONTENT : begin -->
					<div id="page-content">

						
						<h2>Example of creating Modals with Twitter Bootstrap</h2>

						<!-- PRICING TABLE SECTION : begin -->
						<section class="c-section">

							<h2>Specials(EXAMS,CATS preps and Live Tutorials)</h2>

							<div class="row">
								<div class="col-sm-4">

									<!-- PRICING TABLE : begin -->
									<div class="c-pricing-table">
										<h4 class="table-title">EXAM prep</h4>
										<div class="table-content">
											<h5 class="table-price">$70<span>per hour</span></h5>
											<p>With our exams prep,we gurantee you good grades.</p>
											<p><a data-toggle="modal" data-target="#myModal" class="c-button m-color-2">Hire Now</a></p>
											
										</div>
									</div>
									<!-- PRICING TABLE : end -->

								</div>
								<div class="col-sm-4">

									<!-- PRICING TABLE : begin -->
									<div class="c-pricing-table">
										<h4 class="table-title">CAT prep</h4>
										<div class="table-content">
											<h5 class="table-price">$40<span>per hour</span></h5>
											<p>We all know good cat grades are essential for a high cumulative GPA</p>
											<p><a data-toggle="modal" data-target="#myModal" class="c-button m-color-2">Hire Now</a></p>
										</div>
									</div>
									<!-- PRICING TABLE : end -->

								</div>
								<div class="col-sm-4">

									<!-- PRICING TABLE : begin -->
									<div class="c-pricing-table">
										<h4 class="table-title">Live tutoring(SKYPE)</h4>
										<div class="table-content">
											<h5 class="table-price">$127<span>per hour</span></h5>
											<p>You get to sit down with a real proffesor and get tought one on one via skype</p>
											<p><a data-toggle="modal" data-target="#myModal" class="c-button m-color-2">Hire Now</a></p>
										</div>
									</div>
									<!-- PRICING TABLE : end -->

								</div>
							</div>

							<hr class="c-divider m-transparent">
                           
							<h2>Assignments and writtings</h2>
							<p>
							These tables show <b>prices per page</b>, they are different depending on the college level of the assignment and deadline for completion.
							</p>

							<!--our prices start-->
							  <table>
								<thead>
									<tr>
										<th>Work Type</th>
										<th>8 hrs</th>
										<th>24 hrs</th>
										<th>48 hrs</th>
										<th>3 days</th>
										<th>5 days</th>
										<th>7 days</th>
										<th>14 days</th>
									</tr>
								</thead>
							</table>
							
								 <?php

						            $query="SELECT * FROM prices_per_page";
						                $result = mysql_query($query);
						                   
						                    if(!$result)
						                         {
						                          echo "Error ".mysql_error();
						                         }

						                    else{

						                     while($row=mysql_fetch_array($result))
						                     {
						                      $work_type=$row['work_type'];
						                      $hrs_8=$row['8_hrs'];
						                      $hrs_24=$row['24_hrs'];
						                      $hrs_48=$row['48_hrs'];
						                      $days_3=$row['3_days'];
						                      $days_5=$row['5_days'];
						                      $days_7=$row['7_days'];
						                      $days_14=$row['14_days'];
						                  ?>
						                 <table class="table table-striped table-hover"> 
						                   <tbody><tr><td width="18%"><strong><?php echo $work_type;?></strong></td><td><?php echo $hrs_8;?></td><td><?php echo $hrs_24;?></td><td><?php echo $hrs_48;?></td><td><?php echo $days_3;?></td><td><?php echo $days_5;?></td><td><?php echo $days_7;?></td><td><?php echo $days_14;?></td></tr></tbody>
						                 </table>
						              <?php 
						             }
						           }
						        ?>
						        
							<!--our prices end-->

						    <!-- <table>
								<thead>
									<tr>
										<th>Work Type</th>
										<th>8 hrs</th>
										<th>24 hrs</th>
										<th>48 hrs</th>
										<th>3 days</th>
										<th>5 days</th>
										<th>7 days</th>
										<th>14 days</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											<strong>High School</strong><br>
											
										</td>
										<td>$9.00</td>
										<td>$7.00</td>
										<td>$6.00</td>
										<td>$5.50</td>
										<td>$5.50</td>
										<td>$5.50</td>
										<td>$5.50</td>
									</tr>
									<tr>
										<td>
											<strong>College 1-2</strong><br>
										
										</td>
										<td>$14.00</td>
										<td>$12.00</td>
										<td>$11.00</td>
										<td>$10.00</td>
										<td>$10.00</td>
										<td>$10.00</td>
										<td>$10.00</td>
									</tr>
									<tr>
										<td>
											<strong>College 3-4</strong><br>
										
										</td>
										<td>$15.00</td>
										<td>$13.00</td>
										<td>$12.00</td>
										<td>$11.00</td>
										<td>$11.00</td>
										<td>$11.00</td>
										<td>$11.00</td>
									</tr>
									<tr>
										<td>
											<strong>Masters</strong><br>
										
										</td>
										<td>$17.00</td>
										<td>$15.00</td>
										<td>$13.00</td>
										<td>$12.00</td>
										<td>$12.00</td>
										<td>$12.00</td>
										<td>$12.00</td>
									</tr>
									<tr>
										<td>
											<strong>Ph.D.</strong><br>
											
										</td>
										<td>--</td>
										<td>$20.00</td>
										<td>$17.00</td>
										<td>$16.00</td>
										<td>$15.00</td>
										<td>$15.00</td>
										<td>$15.00</td>
									</tr>
								</tbody>
							</table> -->

						</section>
						<!-- PRICING TABLE SECTION : end -->
						
						
						

					</div>
					<!-- PAGE CONTENT : end -->

				</div>

			</div>
			<!-- CORE : end -->


			<?php

include 'mainIncludes/footer.html.php';

?>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">This Modal title</h4>
      </div>
   <div class="modal-body">
        Add some text here
   </div>
   <div class="modal-footer">
     <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
     <button type="button" class="btn btn-primary">Submit changes</button>
   </div>
 </div><!-- /.modal-content -->
</div><!-- /.modal -->