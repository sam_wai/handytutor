<?php

include 'mainIncludes/header.html.php';	
	
?>

	<!-- CORE : begin -->
	<div id="core" class="post-105 lsvrproject type-lsvrproject status-publish hentry">

		<!-- PAGE HEADER : begin -->
<div id="page-header"
	 class="m-parallax">
	 
	 
	 <!-- PAGE TITLE TOP : begin -->
					<div class="page-title-top">
						<div class="page-title-top-inner">

							<!-- PAGE TITLE TEXT : begin -->
							<div class="page-title-text">
								<div class="container">
									<h1>How It Works</h1>
									
								</div>
							</div>
							<!-- PAGE TITLE TEXT : end -->

						</div>
					</div>
					<!-- PAGE TITLE BREADCRUMBS : begin -->
					<div class="page-title-breadcrumbs">
					<ul>
					<li>1. <a href="">Post Assignment</a></li>
					<li>2. <a href="">Get the pricing</a></li>
					<li>3. <a href="">Agree to it</a></li>
					<li>4. <a href="">Pay deposit (40%)</a></li>
					<li>5. <a href="">Get work done</a></li>
					<li>6. <a href="">Pay remaining  and download work</a></li>
					<li>7. <a href="">Leave a review</a> <i class="fa fa-smile-o "></i> </li>
					</ul>
				        </div>
					<!-- PAGE TITLE TOP : end -->
		</div>
	</div>
	<!-- PAGE HEADER TOP : end -->
		<div class="container">

			<!-- PAGE CONTENT : begin -->
			<div id="page-content">

				<!-- PROJECT DETAIL : begin -->
				<div class="project-detail">	
				<img src="images/howitworks/how_it_work.png" alt="img" height="">
				<div class="row">
				</div>
				</div>
				<hr class="c-separator">
				</div>
				<!-- PROJECT DETAIL : end -->

			</div>
			<!-- PAGE CONTENT : end -->
		</div>
	</div>
	<!-- CORE : end -->
<?php

include 'mainIncludes/footer.html.php';

?>
