<?php
include 'configs/config.php';
include 'functions/functions.php';

include 'mainIncludes/header.html.php';
if(isset($_REQUEST['system_msg']) and isset($_REQUEST['user_msg']) ){
	
	$system_msg = $_REQUEST['system_msg'];	
	$user_msg = $_REQUEST['user_msg'];
	
}else{
	
	homeTAker();
	
}

?>	
			<!-- CORE : begin -->
			<div id="core">				
				<!-- PAGE TITLE : begin -->
				<div id="page-title" class="m-parallax">
					<!-- PAGE TITLE TOP : begin -->
					<div class="page-title-top">
						<div class="page-title-top-inner">
							<!-- PAGE TITLE TEXT : begin -->
							<div class="page-title-text">
								<div class="container">
									<h1><strong>404</strong><br><?php
									
										if(DEBUG_MODE){
											
											echo '<font color ="red">
											SYSTEM ERROR :'.$system_msg.'</font><br>';
											echo 'USER MESSAGE :'.$user_msg;
										
										}else{
											
											//this will be excecuted if the debug mode is false
											
											echo $user_msg ;
										}
									
									?></h1>
								</div>
							</div>
							<!-- PAGE TITLE TEXT : end -->

						</div>
					</div>
					<!-- PAGE TITLE TOP : end -->

					<!-- PAGE TITLE BOTTOM : begin -->
					<div class="page-title-bottom">
						<div class="container">
							<div class="page-title-bottom-inner">
								<div class="page-title-bottom-inner2">

									<!-- PAGE TITLE BREADCRUMBS : begin -->
									<div class="page-title-breadcrumbs">
										<ul>
											<li><a href=".">Home</a></li>
											<li>404</li>
										</ul>
									</div>
									<!-- PAGE TITLE BREADCRUMBS : end -->

								</div>
							</div>
						</div>
					</div>
					<!-- PAGE TITLE BOTTOM : end -->

				</div>
				<!-- PAGE TITLE : end -->

				<div class="container">
					
					

				</div>

			</div>
			<!-- CORE : end -->
			
			<?php
			
			include 'mainIncludes/footer.html.php';
			
			?>
