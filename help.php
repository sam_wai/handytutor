<?php

include 'mainIncludes/header.html.php';	
	
?>	

			<!-- CORE : begin -->
			<div id="core">

				<!-- PAGE TITLE : begin -->
				<div id="page-title" class="m-parallax">

					<!-- PAGE TITLE TOP : begin -->
					<div class="page-title-top">
						<div class="page-title-top-inner">

							<!-- PAGE TITLE TEXT : begin -->
							<div class="page-title-text">
								<div class="container">
									<h1>Help and FAQ</h1>
								</div>
							</div>
							<!-- PAGE TITLE TEXT : end -->

						</div>
					</div>
					<!-- PAGE TITLE TOP : end -->

					<!-- PAGE TITLE BOTTOM : begin -->
					<div class="page-title-bottom">
						<div class="container">
							<div class="page-title-bottom-inner">
								<div class="page-title-bottom-inner2">

									<!-- PAGE TITLE BREADCRUMBS : begin -->
									<div class="page-title-breadcrumbs">
										<ul>
											<li><a href="index.php">Home</a></li>
											<li><a href="help.php">Help</a></li>
							
										</ul>
									</div>
									<!-- PAGE TITLE BREADCRUMBS : end -->

								</div>
							</div>
						</div>
					</div>
					<!-- PAGE TITLE BOTTOM : end -->

				</div>
				<!-- PAGE TITLE : end -->

				<div class="container">
					<div class="row">
						<div class="col-md-9 col-md-push-3">

							<!-- PAGE CONTENT : begin -->
							<div id="page-content">

								<div class="row">
									<div class="col-md-6">
									
									<!-- WARNING MESSAGES : begin -->
									<p class="c-alert-message m-info">
									<i class="ico fa fa-info-circle"></i>
									Please search the FAQ's below for any help needed,If you can't 
									find what you're looking for,Please contact us via the contact form provided
									<a href="contactUs.php"><font color="red">Contact us</font></a>
									</p>
									<!--WARNING MESSAGES : end -->

									</div>
								</div>

								<hr class="c-divider m-size-small m-transparent">

								<!-- ACCORDION : begin -->
								<ul class="c-accordion">

									<!-- FAQ : begin -->
									<li>
										<h4 class="accordion-title"><i class="im im-question"></i>Frequently Asked Questions (Click Here to View) </h4>
										<div class="accordion-content">

											<dl>
												<dt><span id="hw"> How does it work?</span></dt>
												<dd>
												How the  site works is explained in clarity and detail <a href="howItworks.php"><font color="red">here</font></a>.
												</dd>
												
												<dt><span id="dc">Does it cost money?</span></dt>
												<dd> 
												<ol>
												 <li>Registration is free</li>
    												 <li>Posting homework is free</li>
    												 <li>Getting your homework done is on downpayment of a fee ( which is 40% of the total price ),the 
    												 rest is paid when &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;collecting the assignment</li>
    												 <li>Previous Answers are priced at 30% less the original price.</li>
    												 <li>You only pay if you decide to buy an answer</li>
    												 </ol>
												</dd>
												
												<dt><span id="hp">I don't have a PayPal account. How can I pay for homework?</span></dt>
												<dd>You can pay with a credit or debit card using PayPal without having a PayPal account and without registering for such an account.
												<ol>
												 <li>Click the Pay Now button of the answer you wish to purchase, you will be redirected to a PayPal web page</li>
    												 <li>Click the 'Pay with a credit or debit card' text below</li>
    												 <li>Proceed by filling in the rest of the information and clicking Agree and Continue at the bottom</li>
    												 </ol>
												</dd>
												<dt><span id="att">How can I attach a file to my homework?</span></dt>
												<dd>
												<ol>
												<li>Find your homework under My homework -> My questions</li>
												<li>Click your homework title</li>
												<li>Click edit</li>
												<li>Scroll down to Attachments and click it</li>
												<li>Choose a file and upload</li>
												<li>Click 'Ask Your Question'</li>
												<ol>
												</dd>
												
												<dt><span id="ddn">I did not get the homework assistance I expected. What should I do?</span></dt>
												<dd>
												This applies to:
												<ol>
												<li>You are not satisfied with an answer you purchased</li>
												<li>You made a down payment to have work done and the teacher assigned your work  did not follow through</li>
												</ol>
												If you are not satisfied with the answer you purchased,a small check of what you realy needed based on what you expected is done.
												if the website management is convinced that you are your query is legite,you would be immediately refunded
												as per the site  <a href=""><font color="red">Terms  and Conditions</font></a> .
												</dd>
												
												<dt><span id="pwd">How do I reset my password?</span></dt>
												<dd>Go to Login -> Request new password and type in your email. You will get an email with a password reset link</dd>	
												
												<dt><span id="plr">Plagiarism checks</span></dt>
												<dd> Here At Handy Tutors plagarism is considered a serious crime!  and thats why we put an end to it. 
												We  maintain a strict guidline of always providing plagarism free paper! <br>
												In addition,you can always get a plagiarism check of an answer before you buy it for just $2.00. Our plagiarism check is powered by Copyscape, a world leading plagiarism checker. </dd>			
			
											</dl>

										</div>
									</li>
									<!-- FAQ : end -->

								</ul>
								<!-- ACCORDION : end -->

							</div>
							<!-- PAGE CONTENT : end -->

						</div>
						<div class="col-md-3 col-md-pull-9">

							<hr class="c-divider m-size-large m-type-2 hidden-lg hidden-md">
							<!-- SIDEBAR : begin -->
							<div id="sidebar">
								<div class="sidebar-widget-list">

									<!-- SERVICES WIDGET : begin -->
									<div class="widget services-widget">
										<div class="widget-inner">
											<h3 class="widget-title"> In faq</h3>
											<ul>
 										                <li class="m-active"><i class="fa fa-question-circle "></i><a href="#hw">How does it work?</a></li>
												<li class="m-active"><i class="fa fa-usd"></i><a href="#dc">Does it cost money?</a></li>
												<li class="m-active"><i class="fa fa-paypal "></i><a href="#hp">I don't have a PayPal account</a></li>
												<li class="m-active"><i class="fa fa-files-o "></i><a href="#att">How can I attach a file to my homework?</a></li>
												<li class="m-active"><i class="fa fa-thumbs-o-down "></i><a href="#ddn">I did not get the homework assistance I expected. What should I do?</a></li>
												<li class="m-active"><i class="fa fa-unlock "></i><a href="#pwd">How do I reset my password?</a></li>
												<li class="m-active"><i class="fa fa-copyright "></i><a href="#plr">Plagiarism checks</a></li>
												<li class="m-active"><i class="fa fa-phone "></i><a href="contactUs.php"><font color="red">Contact Us</font></a></li>
											</ul>
										</div>
									</div>
									<!-- SERVICES WIDGET : end -->

								</div>
							</div>
							<!-- SIDEBAR : end -->

						</div>
					</div>
				</div>

			</div>
			<!-- CORE : end -->

			<?php

include 'mainIncludes/footer.html.php';

?>
