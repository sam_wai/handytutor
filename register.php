<?php

include 'mainIncludes/header.html.php';	
include 'configs/connections.php';
	
?>

		<!-- CORE : begin -->
			<div id="core">

				<!-- PAGE TITLE : begin -->
				<div id="page-title" class="m-parallax">

					<!-- PAGE TITLE TOP : begin -->
					<div class="page-title-top">
						<div class="page-title-top-inner">

							<!-- PAGE TITLE TEXT : begin -->
							<div class="page-title-text">
								<div class="container">
									<h1>Register</h1>
								</div>
							</div>
							<!-- PAGE TITLE TEXT : end -->

						</div>
					</div>
					<!-- PAGE TITLE TOP : end -->

					<!-- PAGE TITLE BOTTOM : begin -->
					<div class="page-title-bottom">
						<div class="container">
							<div class="page-title-bottom-inner">
								<div class="page-title-bottom-inner2">

									<!-- PAGE TITLE BREADCRUMBS : begin -->
									<div class="page-title-breadcrumbs">
										<ul>
											<li><a href="index.php">Home</a></li>
                                            <li><a href="loginRegister.php">Login</a></li>
                                            <li><a href="">Register</a></li>
										</ul>
									</div>
									<!-- PAGE TITLE BREADCRUMBS : end -->

								</div>
							</div>
						</div>
					</div>
					<!-- PAGE TITLE BOTTOM : end -->

				</div>
				<!-- PAGE TITLE : end -->

				<div class="container">

					<!-- PAGE CONTENT : begin -->
					<div id="page-content">

						<!-- SEARCH RESULTS : begin -->
						<div class="search-resultsx">

							<div class="row">
							
							
							<!-- CONTACT FORM : begin class=m-ajax-form-->
						<form id="contact-form" class="default-form" action="" method="POST" >
							<input type="hidden" name="contact-form">
							<!-- FORM VALIDATION ERROR MESSAGE : begin -->
							<p class="c-alert-message m-warning m-validation-error" style="display: none;"><i class="ico fa fa-exclamation-circle"></i>Please fill in all required (*) fields.</p>
							<!-- FORM VALIDATION ERROR MESSAGE : end -->

							<!-- SENDING REQUEST ERROR MESSAGE : begin -->
							<p class="c-alert-message m-warning m-request-error" style="display: none;"><i class="ico fa fa-exclamation-circle"></i>There was a connection problem. Try again later.</p>
							<!-- SENDING REQUEST ERROR MESSAGE : end -->

							<div class="row">
								<div class="col-sm-4">
                                      
								</div>
								<div class="col-sm-4">
                                  <!--verify and insert start-->
                                   <?php

		                            if(isset($_POST['submit']) && $_POST['email']!="" && $_POST['username']!="")
		                                 {
		       
		                                 $emailadd=mysql_real_escape_string($_POST['email']);
		                                 $username=mysql_real_escape_string($_POST['username']);

		                                 $find="SELECT email FROM users WHERE email='$emailadd'";

		                                 $result=mysql_query($find) or die(mysql_error());


		                            if(mysql_num_rows($result)!=0)
		                               {
		                                echo "<div id='alert_login'>*The <b>email address</b> already in use.Kindly reset password if forgotten.</div>";
		                                echo "<br>";
		                               }
		                            else
		                              {
		                              	//generates random password
		                                function randomPass($length)
		                                  {
		                                    $password="";
		                                    $length=$length;
		                                    $chars="abcdefghijklmnopqrstuvwxyz123456789";

		                                  for($x=1;$x<=$length;$x++)
		           
		                                     {
		                                    $rand=rand() %strlen($chars);
		                                    $temp=substr($chars,$rand,1);
		                                    $password.=$temp;
		                                    }
		                                return $password;
		                               }
		  
		  
						                $pass=randomPass(7);
						                //$pas1=md5($pass);

						                   //insert to the db
						                 
									     $querry=mysql_query("INSERT INTO users (username,email,password,date_joined)
									             VALUES ('$username','$emailadd','$pass',NOW())");
									     //catches the errors
									    if(!$querry)
									        {
									           echo "Could not insert ".mysql_error();
									         
									        }

									    else    //send email and redirect to homepage
									          {
									          
									         
									           $_SESSION['logged_user'] = $_POST['email']; 
									           echo "Welcome ".$_POST['username'].",";
									          
									           echo "Successfully Registered!.";
									            
									              $to = $_POST['email'];
									              $user=$_POST['username'];
									              $subject = "Handytutor \n loggin password";
									              $message = "Hello $user,\n The Password is $pass .Use it to loggin to our site.\n\nThis is automatically sent Email.Please do not reply.";
									              $header="Handytutor";
									                //mail($to,$subject,$message,$header);

									                echo '<script type="text/javascript">'; 
													echo 'window.location.href = "postHomework.php";';
													echo '</script>';
											     } 
								               }
								             }
				              
		                               ?>
                                    <!--verify and insert end-->
									<!-- SUBJECT FIELD : begin -->
									<div class="form-field">
										<label for="contact-subject">Username</label>
										<input id="contact-subject" name="username" placeholder="Username.." required>
									</div>
									
									<div class="form-field">
										<label for="contact-subject">Email adress</label>
										<input id="contact-subject" name="email" type="email" placeholder="Email address.." required>
									</div>
									
									<!-- SUBJECT FIELD : end -->
									<!-- SUBMIT BUTTON : begin -->
									<div class="form-field">
										<button class="c-button m-type-2 m-color-2" type="submit" name="submit" data-label="Send Message" data-loading-label="Veryfying...">Register</button>
									</div>
									<!-- SUBMIT BUTTON : end -->
                                   <span style="font-weight:bold;color:#f2f30">*NB The  login password is sent to your email address</span>
								</div>
							</div>

						</form>
						<!-- CONTACT FORM : end -->
						
							
							
							</div>

						</div>
						<!-- SEARCH RESULTS : end -->

					</div>
					<!-- PAGE CONTENT : end -->

				</div>

			</div>
			<!-- CORE : end -->

			<?php

    include 'mainIncludes/footer.html.php';
?>
