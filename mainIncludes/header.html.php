<!DOCTYPE html>
<?php 
include 'configs/connections.php';
session_start();//start session on all pages
?>
<html>	
<head>
        <script src="library/js/jquery-1.9.1.min.js" type="text/javascript"></script>

		<meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Handy Tutors</title>
        <link rel="shortcut icon" href="images/favicon.png">

		<!-- GOOGLE FONTS : begin -->
		<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic%7cExo+2:400,700&amp;subset=latin,latin-ext" rel="stylesheet" type="text/css">
		<!-- GOOGLE FONTS : end -->

        <!-- STYLESHEETS : begin -->
        <link rel="stylesheet" href="date_picker/pikaday.css">
		<link rel="stylesheet" type="text/css" href="library/css/style.css">
        <link rel="stylesheet" type="text/css" href="library/css/skin/default.css">
		<link rel="stylesheet" type="text/css" href="library/css/custom.css">
		<link rel="stylesheet" type="text/css" href="library/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="library/css/bootstrap-theme.min.css">
		<!-- STYLESHEETS : end -->

        <!--[if lte IE 8]>
			<link rel="stylesheet" type="text/css" href="library/css/oldie.css">
			<script src="library/js/respond.min.js" type="text/javascript"></script>
        <![endif]-->
		<script src="library/js/modernizr.custom.min.js" type="text/javascript"></script>
		<script src="library/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="library/js/jquery-1.11.1.min.js" type="text/javascript"></script>

	</head>
	<body class="m-fixed-header">

		<!-- WRAPPER : begin -->
		<div id="wrapper">

			<!-- HEADER : begin -->
			<header id="header">
				<div class="header-inner">
					<div class="container">

						<!-- HEADER BRANDING : begin -->
						<div class="header-branding">
							<div class="branding-inner">

								<!-- BRANDING LOGO : begin -->
								<div class="brading-logo">
									<a href="."><img src="images/index/handy.jpeg"></a>
								</div>
								<!-- BRANDING LOGO : end -->

								<!-- BRANDING INFO : begin -->
								<div class="brading-info">
									
									<strong>HANDY TUTORS	</strong><br>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<a href=".">
									<em>Helping Students</em>
									</a>
								</div>
								<!-- BRANDING INFO : end -->

							</div>
						</div>
						<!-- HEADER BRANDING : end -->

						<!-- NAVIGATION TOGGLE : begin -->
						<button class="header-navigation-toggle" type="button"><i class="fa fa-bars"></i></button>
						<!-- NAVIGATION TOGGLE : end -->

						<!-- HEADER NAVIGATION : begin -->
						<div class="header-navigation">
							<div class="navigation-inner">

								<!-- HEADER MENU : begin -->
								<nav class="header-menu">
									<ul>
										<li ><!-- class="m-active" -->
											<a href="index.php">Home</a>
				
										</li>
										<li ><!-- class="m-active" -->
											<a href="postHomework.php">Post Homework</a>
				
										</li>
										<li>
											<a href="previousAnswers.php">Previous Answers</a>
											<ul>
												<li ><a href="<?php echo 'previousAnswers.php?id=1';?>">Accounting, Business & Finance</a></li>
												<li ><a href="<?php echo 'previousAnswers.php?id=2';?>"> Agriculture and Horticulture</a></li>
												<li ><a href="<?php echo 'previousAnswers.php?id=3';?>"> Architecture, Building & Planning</a></li>
												<li ><a href="<?php echo 'previousAnswers.php?id=4';?>">Art and design</a></li>
												<li ><a href="<?php echo 'previousAnswers.php?id=5';?>">Biology</a></li>
												<li ><a href="<?php echo 'previousAnswers.php?id=6';?>">Chemistry</a></li>
												<li ><a href="<?php echo 'previousAnswers.php?id=7';?>">Communication and Media</a></li>
												<li ><a href="<?php echo 'previousAnswers.php?id=8';?>">Computing and IT</a></li>
												<li ><a href="<?php echo 'previousAnswers.php?id=9';?>">Economics</a></li>
												<li ><a href="<?php echo 'previousAnswers.php?id=10';?>">Education</a></li>
												<li ><a href="<?php echo 'previousAnswers.php?id=11';?>">Engineering</a></li>
												<li ><a href="<?php echo 'previousAnswers.php?id=12';?>">English Language & Literature</a></li>
												<li ><a href="<?php echo 'previousAnswers.php?id=13';?>">Geography</a></li>
												<li ><a href="<?php echo 'previousAnswers.php?id=14';?>">Health and Medicine</a></li>
												<li ><a href="<?php echo 'previousAnswers.php?id=15';?>">History</a></li>
												<li ><a href="<?php echo 'previousAnswers.php?id=16';?>">Human Resource Management</a></li>
												<li ><a href="<?php echo 'previousAnswers.php?id=17';?>">Hospitality and Catering</a></li>
												<li ><a href="<?php echo 'previousAnswers.php?id=18';?>">Law</a></li>
												<li ><a href="<?php echo 'previousAnswers.php?id=19';?>">Mathematics</a></li>
												<li ><a href="<?php echo 'previousAnswers.php?id=20';?>">Management</a></li>
												<li ><a href="<?php echo 'previousAnswers.php?id=21';?>">Marketing</a></li>
												<li ><a href="<?php echo 'previousAnswers.php?id=22';?>">Physics</a></li>
												<li ><a href="<?php echo 'previousAnswers.php?id=23';?>">Politics</a></li>
												<li ><a href="<?php echo 'previousAnswers.php?id=24';?>">Psychology and Counselling</a></li>
											</ul>
	
										</li>
										<li>
											<a href="ourRates.php">Our Prices</a>
										</li>
										<li>
											<a href="loginRegister.php">Login/Register</a>
											
										</li>
										<li><a href="howItworks.php">How it works</a></li>
										<li><a href="help.php">Help</a></li>
						
									</ul>
								</nav>
								<!-- HEADER MENU : end -->

								<!-- HEADER SEARCH : begin -->
								<div class="header-search">
									<form action="index.php?method='search">
										<input class="search-input" type="text" placeholder="Search for...">
										<button class="search-submit" type="submit"><i class="fa fa-chevron-right"></i></button>
									</form>
									<button class="search-toggle" type="button"><i class="fa fa-search"></i></button>
								</div>

								<!-- HEADER SEARCH : end -->
								

							</div>

						</div>

						<!-- HEADER NAVIGATION : end -->
                        
					</div>
                    <!--logged in user-->
								<div class="logged_in">
								  <?php 
								    if($_SESSION['logged_user'])
								       {
						                $user=$_SESSION['logged_user'];
						                $query="SELECT username FROM users WHERE email='$user'";
						                $result = mysql_query($query);

						                if(!$result)
						                    {
						                          echo "Error ".mysql_error();
						                    }

						                else{

							                  while($row=mysql_fetch_array($result)){
							                    $theuser=$row['username'];
							                      ?>
							                       <div class="dropdown">					            							                  
							                        
							                            <img src="images/user.jpg" height="52px" alt="Avatar" style="margin-left:px;"><br>
							                        
							                        
							                        <a class="dropdown-toggle" href="#" data-toggle="dropdown" id="dropdown1">
							                        <span class="name"><?php echo $theuser; ?><span class="caret"></span></span></a>
							                        <ul class="dropdown-menu pull-right list-unstyled" role="menu" aria-labelledby="dropdown1">
							                            <li><a href="my_account.php">Profile</a></li>
							                           <!--  <li><a href="#"> Account Settings</a></li> -->
							                            <li><a href="logout.php"> <span style="color:#f00">Logout</span></a></li>
							                        </ul><br>
							                        
							                       </div>
							               <?php
							                  }
						              }
						         }
								   ?>
								</div>
								<!--logged in user end-->
				</div>

			</header>
			<!-- HEADER : end -->
	</body>
</head>
						
