
<!-- FOOTER : begin -->
			<div id="footer" class="m-parallax">

				<!-- FOOTER INNER : begin -->
				<div class="footer-inner">
				
					<div class="container">

						

							<div class="col-md-6 col-md-push-6">

								<!-- FOOTER MENU : begin -->
								<nav class="footer-menu">
									<ul>
										<li><a href="index.php">Home</a></li>
										<li><a href="postHomework.php">Post homework</a></li>
										<li><a href="help.php">Help</a></li>
									</ul>
								</nav>
								<!-- FOOTER MENU : end -->

							</div>

					</div><br><br>
<div class='copy'>
Copyright © &nbsp;<?php echo date('Y')  ?> &nbsp;Handytutors.com All rights reserved.<br>
Uploading copyrighted material is not allowed.
All homework assistance is provided by our tutors under student agreement on Our SLA terms and conditions<br>.Handytutor will no be held liable for 
any inconvinience or problem that will arise from its assistance.
</div>
				</div>
				<!-- FOOTER INNER : end -->

			</div>
			<!-- FOOTER : end -->

		</div>
		<!-- WRAPPER : END -->

		<!-- SCRIPTS : begin -->
		<script src="library/js/jquery-1.9.1.min.js" type="text/javascript"></script>
		<script src="library/js/third-party.js" type="text/javascript"></script>
		<script src="library/js/library.js" type="text/javascript"></script>
		<script src="library/js/scripts.js" type="text/javascript"></script>
		<!-- SCRIPTS : end -->

	</body>

<!-- Mirrored from demos.volovar.net/bluecollar.html/demo/ by HTTrack Website Copier/3.x [XR&CO'2013], Tue, 27 Jan 2015 09:05:06 GMT -->
</html>
