<?php

include 'mainIncludes/header.html.php';	
	
?>

			<!-- CORE : begin -->
			<div id="core">

				<!-- PAGE TITLE : begin -->
				<div id="page-title" class="m-parallax">

					<!-- PAGE TITLE TOP : begin -->
					<div class="page-title-top">
						<div class="page-title-top-inner">

							<!-- PAGE TITLE TEXT : begin -->
							<div class="page-title-text">
								<div class="container">
									<h1>Contact Us</h1>
								</div>
							</div>
							<!-- PAGE TITLE TEXT : end -->

						</div>
					</div>
					<!-- PAGE TITLE TOP : end -->

					<!-- PAGE TITLE BOTTOM : begin -->
					<div class="page-title-bottom">
						<div class="container">
							<div class="page-title-bottom-inner">
								<div class="page-title-bottom-inner2">

									<!-- PAGE TITLE BREADCRUMBS : begin -->
									<div class="page-title-breadcrumbs">
										<ul>
											<li><a href="index.php">Home</a></li>
											<li>Contact Us</li>
										</ul>
									</div>
									<!-- PAGE TITLE BREADCRUMBS : end -->

								</div>
							</div>
						</div>
					</div>
					<!-- PAGE TITLE BOTTOM : end -->

				</div>
				<!-- PAGE TITLE : end -->

				<div class="container">

					<!-- PAGE CONTENT : begin -->
					<div id="page-content">

						<div class="row">
							<div class="col-md-4">

								<hr class="c-divider">
                                 <h2>Talk to us now</h2>
								<hr class="c-divider">

								<!-- ICON BLOCK : begin -->
								<div class="c-icon-block">
									<i class="im im-phone"></i>
									<h3>24/7 Support</h3>
			
									<a href="#">help@handytutors.com</a></p>
								</div>
								<!-- ICON BLOCK : end -->

								<hr class="c-divider m-transparent hidden-lg hidden-md">

							</div>
							 
							
							<div class="col-md-8">
                               <hr class="c-divider">
                                 <h2>Find us</h2>
								<hr class="c-divider">	
								<!-- GOOGLE MAP : begin -->
								<!-- If you specify the "data-address" attribute, "data-latitude" and "data-longitude" are optional -->
								<div class="c-gmap"
									data-address="8833 Sunset Blvd, West Hollywood, CA 90069, USA"
									data-latitude="34.0909191"
									data-longitude="-118.38417279999999"
									data-zoom="18"
									data-maptype="SATELLITE"
									data-enable-mousewheel="false">
								</div>
								<!-- GOOGLE MAP : end -->

							</div>
						</div>

						<hr class="c-divider m-size-medium">

						<!-- CONTACT FORM : begin -->
						<form id="contact-form" class="default-form" action="" method="post" >
							<input type="hidden" name="contact-form">

							<!-- FORM VALIDATION ERROR MESSAGE : begin -->
							<p class="c-alert-message m-warning m-validation-error" style="display: none;"><i class="ico fa fa-exclamation-circle"></i>Please fill in all required (*) fields.</p>
							<!-- FORM VALIDATION ERROR MESSAGE : end -->

							<!-- SENDING REQUEST ERROR MESSAGE : begin -->
							<p class="c-alert-message m-warning m-request-error" style="display: none;"><i class="ico fa fa-exclamation-circle"></i>There was a connection problem. Try again later.</p>
							<!-- SENDING REQUEST ERROR MESSAGE : end -->

                                 <h2>Write to us now</h2>
							<hr class="c-divider">
							 <?php

		                            if(isset($_POST['send']) && $_POST['message'] !="")
		                                 {
	
						                
						                   //insert to the db
						                 
									     $querry=mysql_query("INSERT INTO messages (subject,message,username,email)
									             VALUES ('$_POST[subject]','$_POST[message]','$_POST[username]','$_POST[email]')");
									     //catches the errors
									    if(!$querry)
									        {
									           echo "Could not insert ".mysql_error();
									         
									        }

									    else    //send email 
									          {
									          
									         
									           echo "Successfully sent!";
							                       //email to admin
									              $to = "admin@handytutors.com";
									              $user="HandyTutor";
									              $subject = $_POST['subject'];
									              $message = $_POST['message']." \n\nsent by\n ".$_POST['email'];
									              $header="Handytutor";
									                //mail($to,$subject,$message,$header);
		            
											     } 
								               
								             }
				              
		                               ?>
							<hr class="c-divider">
                                 

							<div class="row">
								<div class="col-sm-4">

									<!-- NAME FIELD : begin -->
									<div class="form-field">
										<label for="contact-name">Your Username <span>*</span></label>
										<input id="contact-name" name="username" type="text" class="m-required" required>
									</div>
									<!-- NAME FIELD : end -->

									<!-- EMAIL FIELD : begin -->
									<div class="form-field">
										<label for="contact-email">Your Email Address <span>*</span></label>
										<input id="contact-email" name="email" type="email" class="m-required m-email" required>
									</div>
									<!-- <p style="display: none;">
										<label for="contact-email-hp">Re Email Address</label>
										<input id="contact-email-hp" name="con_email">
									</p> -->
									<!-- EMAIL FIELD : end -->

									<!-- PHONE FIELD : begin -->
					
									<!-- PHONE FIELD : end -->

								</div>
								<div class="col-sm-8">

									<!-- SUBJECT FIELD : begin -->
									<div class="form-field">
										<label for="contact-subject">Message Subject</label>
										<input id="contact-subject" name="subject" type="text" class="m-required" required>
									</div>
									<!-- SUBJECT FIELD : end -->

									<!-- MESSAGE FIELD : begin -->
									<div class="form-field">
										<label for="contact-message">Message <span>*</span></label>
										<textarea id="contact-message" name="message" type="text" class="m-required" required></textarea>
									</div>
									<!-- MESSAGE FIELD : end -->

									<!-- SUBMIT BUTTON : begin -->
									<div class="form-field">
										<button class="submit-btn c-button" name="send" type="submit" data-label="Send Message" data-loading-label="Sending...">Send Message</button>
									</div>
									<!-- SUBMIT BUTTON : end -->

								</div>
							</div>

						</form>
						<!-- CONTACT FORM : end -->

					</div>
					<!-- PAGE CONTENT : end -->

				</div>

			</div>
			<!-- CORE : end -->

			<?php

include 'mainIncludes/footer.html.php';

?>
