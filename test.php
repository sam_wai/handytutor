<?php


<?php
$max = (int) 60;
$string = 'I need this string to be broken apart so that no line is never more than $max characters long.';
$string .= '  However, I only want this string "broken" in/at/on the spaces.';
$in_pieces = array();

while (strlen($string) > $max) {
    // location of last space in the first $max characters
    $substring = substr($string, 0, $max);
    // pull from position 0 to $substring position
    $in_pieces[] = trim( substr( $substring, 0, strrpos($substring, ' ') ) );
    // $string (haystack) now = haystack with out the first $substring characters
    $string = substr($string, strrpos($substring, ' '));
    // UPDATE (2013 Oct 16) - if remaining string has no spaces AND has a string length
    //  greater than $max, then this will loop infinitely!  So instead, just have to bail-out (boo!)
    if (strlen($string) > $max) break;
}
$in_pieces[] = trim($string); // final bits o' text
echo "\n\n" . var_export($in_pieces, TRUE) . "\n\n";





?>
