<?php

include 'mainIncludes/header.html.php';	
	
?>

			<!-- CORE : begin -->
			<div id="core">

				<!-- PAGE TITLE : begin -->
				<div id="page-title" class="m-parallax">

					<!-- PAGE TITLE TOP : begin -->
					<div class="page-title-top">
						<div class="page-title-top-inner">

							<!-- PAGE TITLE TEXT : begin -->
							<div class="page-title-text">
								<div class="container">
									<h1>Our Questions & Previous Answers</h1>
								</div>
							</div>
							<!-- PAGE TITLE TEXT : end -->

						</div>
					</div>
					<!-- PAGE TITLE TOP : end -->

					<!-- PAGE TITLE BOTTOM : begin -->
					<div class="page-title-bottom">
						<div class="container">
							<div class="page-title-bottom-inner">
								<div class="page-title-bottom-inner2">

									<!-- PAGE TITLE BREADCRUMBS : begin -->
									<div class="page-title-breadcrumbs">
										<ul>
											<li><a href="index.php">Home</a></li>
											<li><a href="">Previous Answers</a></li>
										</ul>
									</div>
									<!-- PAGE TITLE BREADCRUMBS : end -->

								</div>
							</div>
						</div>
					</div>
					<!-- PAGE TITLE BOTTOM : end -->

				</div>
				<!-- PAGE TITLE : end -->

				<div class="container">

					<!-- PAGE CONTENT : begin -->
					<div id="page-content">

						<!-- SEARCH RESULTS PAGE : begin -->
						<div class="search-results-page">

							<!-- SEARCH FORM : begin -->
							<form class="c-search-form" action="http://demos.volovar.net/bluecollar.html/demo/search-results.html">
								<div class="form-fields">
									<input type="text" data-placeholder="Lorem Ipsum">
									<button type="submit"><i class="fa fa-search"></i></button>
								</div>
							</form>
							<!-- SEARCH FORM : end -->

							<hr class="c-divider m-size-medium">

							<h2>10 Results for <strong>"Lorem Ipsum"</strong></h2>

							<!-- RESULT ITEM : begin -->
							<h3 class="item-title">Aliquam lorem ante, dapibus in, viverra quis</h3>
							<p class="item-link"><a href="#">http://demos.volovar.net/vibes/demo/lorem-ipsum.html</a></p>
							<div class="item-text various-content">
								<p>Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. <span class="search-query">Lorem Ipsum</span>. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim.</p>
							</div>
							<!-- RESULT ITEM : end -->

							<hr class="c-divider">

							<!-- PAGINATION : begin -->
							<div class="c-pagination">
								<ul>
									<li class="pagination-prev"><a href="#" class="c-button m-outline"><i class="fa fa-chevron-left"></i></a></li>
									<li><a href="#" class="c-button m-outline">1</a></li>
									<li class="m-active"><a href="#" class="c-button m-outline">2</a></li>
									<li><a href="#" class="c-button m-outline">3</a></li>
									<li class="pagination-next"><a href="#" class="c-button m-outline"><i class="fa fa-chevron-right"></i></a></li>
								</ul>
							</div>
							<!-- PAGINATION : end -->

						</div>
						<!-- SEARCH RESULTS PAGE : end -->

					</div>
					<!-- PAGE CONTENT : end -->

				</div>

			</div>
			<!-- CORE : end -->

			<?php

include 'mainIncludes/footer.html.php';

?>
